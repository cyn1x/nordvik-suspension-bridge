#ifndef NORDVIK_H
#define NORDVIK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "sem_ops.h"

#define REQUIRED_ARGS 2
#define NUM_DIRECTIONS 2
#define EAST "EAST"
#define WEST "WEST"
#define TRUCK "TRUCK"
#define CAR "CAR"
#define MAX_WAIT 5
#define CROSSING_TIME 4

volatile int vehicle_sem;
volatile int bridge_sem;
volatile int east_vehicles;
volatile int west_vehicles;
volatile int bridge_count;

/**
 * vehicle - Data relevant to the entire program scope.
 * @type:           Vehicle class.
 * @direction:      Cardinal direction the vehicle is heading.
 * @id:             Unique ID of the vehicle class.
 * @next_vehicle:   Next vehicle in the linked list.
 */
struct vehicle {
    char *type;
    char *direction;
    unsigned int id;
    unsigned int num_vehicles;
    struct vehicle *next_vehicle;
};

int parse_args(struct vehicle *vll, int argc, char **argv);
void initialise_semaphores();
void queue_vehicles(struct vehicle *vll);
struct vehicle *create_vehicle();
void approach_bridge(struct vehicle *vll);
void *manage_traffic(void *arg);
void wait(struct vehicle *vll, pthread_t *t_ids);
void handle_truck(struct vehicle *t);
void handle_car(struct vehicle *c);
void enter_bridge(struct vehicle *c);
void exit_bridge(struct vehicle *c);
void cross_bridge(struct vehicle *v);
void free_memory(struct vehicle *node);
void clean();

#endif
