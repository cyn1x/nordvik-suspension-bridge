COMPILER = gcc
CFLAGS = -Wall -pedantic -lpthread

EXES = nordvik

all: ${EXES}

nordvik:  nordvik.c
	${COMPILER} ${CFLAGS}  nordvik.c -o nordvik
clean:
	rm -f *~ *.o ${EXES}
