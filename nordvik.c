/*
 * Nordvik Suspension Bridge is a single lane bridge that demonstrates mutual
 * exclusion and thread synchronisation through the use of semaphores. The
 * program starts by creating different numbers of threads representing cars
 * and trucks crossing the bridge. The direction of the vehicles is determined
 * by a random pattern to diversify the results. Their crossing of the bridge
 * are synchronised by the rules below.
 *
 * 1. There can be more than a single car on the bridge only if they are
 *    travelling in the same cardinal direction.
 * 2. There can only be a single truck on the bridge at one time, regardless
 *    of the trucks behind it wanting to travel in the same direction.
 * 3. Vehicles can only traverse the bridge if there is no oncoming traffic.
 *
 * Author: Zac Vukovic (zmadden@myune.edu.au)
 *
 * The purpose of this program is to
 *
 * - nordvik [vehicles]
 * - [vehicles]: The number of vehicles to approach the bridge from either side.
 *
 * Example usage:
 * $ make
 * $ nordvik 10
 *
 * Example output:
 * TRUCK 0 going WEST on the bridge.
 * TRUCK 0 going WEST off the bridge.
 * TRUCK 1 going WEST on the bridge.
 * TRUCK 1 going WEST off the bridge.
 * CAR 0 going EAST on the bridge.
 * CAR 0 going EAST off the bridge.
 * TRUCK 2 going EAST on the bridge.
 * TRUCK 2 going EAST off the bridge.
 * CAR 4 going WEST on the bridge.
 * CAR 4 going WEST off the bridge.
 * CAR 2 going WEST on the bridge.
 * CAR 2 going WEST off the bridge.
 * CAR 3 going EAST on the bridge.
 * CAR 5 going EAST on the bridge.
 * CAR 5 going EAST off the bridge.
 * CAR 3 going EAST off the bridge.
 * TRUCK 3 going EAST on the bridge.
 * TRUCK 3 going EAST off the bridge.
 * CAR 1 going WEST on the bridge.
 * CAR 1 going WEST off the bridge.
 *
 * Process finished with exit code 0
 *
 * $
 */

#include "nordvik.h"

/**
 * main() - Program entry point.
 * @argc:   Number of arguments given
 * @argv:   Arguments given to the program
 *
 * Return:  0.
 */
int main(int argc, char **argv)
{
        int valid;
        struct vehicle vll = {0, 0, 0, 0,
                NULL};

        valid = parse_args(&vll, argc, argv);

        if (valid < 0) {
                exit(EXIT_FAILURE);
        }

        initialise_semaphores();
        queue_vehicles(&vll);
        approach_bridge(&vll);
        clean(&vll);

        return 0;
}

/**
 * parse_args() - Check validity of program arguments.
 * @vll:    Linked list of queued vehicles
 * @argc:   Number of arguments passed to the program
 * @argv:   The arguments given to the program
 *
 * Return:  0.
 */
int parse_args(struct vehicle *vll, int argc, char **argv)
{
        if (argc != REQUIRED_ARGS) {
                printf("Error: Invalid argument. %i argument(s) "
                       "entered; %i arguments required.\n",
                       argc, REQUIRED_ARGS);
                printf("Usage: Specify the number of vehicles for this "
                       "demonstration.\n");
                return -1;
        }
        if (atoi(argv[1]) < 1) {
                printf("Error: Invalid argument. %s is not a valid "
                       "number of vehicles.\n", argv[1]);
                printf("Usage: Specify at least 1 vehicle to run the "
                       "program.\n");
                return -1;
        }

        vll->num_vehicles = 10;

        return 0;
}

/**
 * initialise_semaphores() - Initialises semaphores for use.
 *
 * Interprocess Communication can only take part within this process. The
 * semaphores are initialised within the process and released.
 */
void initialise_semaphores()
{
        vehicle_sem = semtran(IPC_PRIVATE);
        bridge_sem = semtran(IPC_PRIVATE);

        V(vehicle_sem);
        V(bridge_sem);
}

/**
 * queue_vehicles() - Adds vehicles to the queue.
 * @vll   Linked list of vehicles added to the queue
 *
 * The Linked List expands the queue data structure by allowing nodes to be
 * removed in a non-orderly fashion.
 */
void queue_vehicles(struct vehicle *vll)
{
        struct vehicle *head = vll;
        time_t t;

        srand(time(&t));

        for (unsigned int i = 0; i < vll->num_vehicles; i++) {
                struct vehicle *new_vehicle = create_vehicle();

                new_vehicle->next_vehicle = head->next_vehicle;
                head->next_vehicle = new_vehicle;
        }
}

/**
 * create_vehicle() - Creates a vehicle with an associated cardinal direction.
 *
 * Return:  New vehicle with a type and direction.
 */
struct vehicle *create_vehicle()
{
        struct vehicle *new_vehicle = malloc(sizeof(struct vehicle));

        new_vehicle->direction = rand() % NUM_DIRECTIONS == 0 ? EAST : WEST;
        new_vehicle->type = rand() % NUM_DIRECTIONS == 0 ? CAR : TRUCK;
        new_vehicle->next_vehicle = NULL;

        return new_vehicle;
}

/**
 * approach_bridge() - Assigns vehicles with a thread to cross the bridge.
 * @vll     Linked list of queued vehicles.
 *
 * Cars and Trucks are given a unique ID for their particular vehicle class.
 * They are then assigned a thread to cross the bridge. The parent process
 * then waits for the threads to finish.
 */
void approach_bridge(struct vehicle *vll)
{
        pthread_t thread_ids[vll->num_vehicles];
        struct vehicle *node = vll;
        unsigned int c_id = 0, t_id = 0;

        for (unsigned int i = 0; i < vll->num_vehicles; i++) {
                node = node->next_vehicle;

                if (strcmp(node->type, TRUCK) == 0) {
                        node->id = t_id;
                        t_id++;
                } else if (strcmp(node->type, CAR) == 0) {
                        node->id = c_id;
                        c_id++;
                }

                pthread_create(&thread_ids[i], NULL, manage_traffic,
                               (void *) node);
        }

        wait(vll, thread_ids);
}

/**
 * wait() - Wait for threads to finish and remove vehicles from the queue.
 * @param vll       Linked list of queued vehicles.
 * @param t_ids     Array containing all threads in the program.
 *
 * The parent process waits for all threads to complete, then initiates their
 * vehicles to be removed from the linked list.
 */
void wait(struct vehicle *vll, pthread_t *t_ids)
{
        struct vehicle *node = vll;
        for (unsigned int i = 0; i < vll->num_vehicles; i++) {
                pthread_join(t_ids[i], NULL);
                free_memory(node);
        }
}

/**
 * manage_traffic() - Mediates traffic depending on the vehicle class.
 * @arg     The vehicle that the thread is managing.
 *
 * Trucks and cars both differ in operation, so a conditional must direct
 * them to the appropriate instruction. A short, random sleep time is used to
 * simulate the effect of queueing.
 *
 * Return:  NULL
 */
void *manage_traffic(void *arg)
{
        sleep(rand() % MAX_WAIT);

        struct vehicle *vehicle = arg;

        if (strcmp(vehicle->type, TRUCK) == 0) {
                handle_truck(vehicle);
                return NULL;
        }

        handle_car(vehicle);

        return NULL;
}

/**
 * handle_truck() - Give trucks mutual exclusion to the critical section.
 * @param t     Truck.
 *
 * That is, a writer gets exclusive access to the critical section, locking out
 * all other vehicles. Using pthreads, the semaphores are implemented simply
 * as pthread mutexes.
 */
void handle_truck(struct vehicle *t)
{
        P(vehicle_sem);

        cross_bridge(t);

        printf("%s %i going %s off the bridge.\n", t->type, t->id,
               t->direction);

        V(vehicle_sem);
}

/**
 * handle_car() - Determine if cars can reach the critical section.
 * @param c     Car.
 *
 * The first car attempting access gains access to the data on behalf of all
 * cars that follow.
 */
void handle_car(struct vehicle *c)
{
        while (1) {
                P(bridge_sem);

                if ((strcmp(c->direction, EAST) == 0) && west_vehicles > 0 && bridge_count > 0) {
                        V(bridge_sem);
                        continue;
                }
                else if ((strcmp(c->direction, WEST) == 0) && east_vehicles > 0 && bridge_count > 0) {
                        V(bridge_sem);
                        continue;
                }

                break;
        }

        enter_bridge(c);

        V(bridge_sem);
        cross_bridge(c);
        P(bridge_sem);

        exit_bridge(c);

        V(bridge_sem);
}

/**
 * enter_bridge() - Increments the number of vehicles currently on the bridge.
 * @param c     Car.
 */
void enter_bridge(struct vehicle *c)
{
        bridge_count++;

        (strcmp(c->direction, EAST) == 0) ? east_vehicles++ : west_vehicles++;

        if (bridge_count == 1)
                P(vehicle_sem);
}

/**
 * exit_bridge() - Decrements the number of vehicles currently on the bridge.
 * @param c     Car.
 */
void exit_bridge(struct vehicle *c)
{
        printf("%s %i going %s off the bridge.\n", c->type, c->id,
               c->direction);

        bridge_count--;
        (strcmp(c->direction, EAST) == 0) ? east_vehicles-- : west_vehicles--;

        if (bridge_count == 0)
                V(vehicle_sem);
}

/**
 * cross_bridge() - The critical section of the program.
 * @param v     Vehicle.
 *
 * Once threads are permitted to access the critical section, they will produce
 * output displaying that they are indeed in the critical section. A short,
 * random sleep time is used to simulate the effect of queueing.
 */
void cross_bridge(struct vehicle *v)
{
        sleep(CROSSING_TIME);
        printf("%s %i going %s on the bridge.\n", v->type, v->id, v->direction);
}

/**
 * free_memory() - Removes a node from a given linked list.
 * @param node      The node in the linked list.
 *
 * The next node in the chain is now the head of the list, where the previous
 * head is removed. This is done sequentially because threads are assigned
 * nodes in sequential order. Once the first thread has completed its task,
 * then the memory from the first node is released back to the operating system.
 */
void free_memory(struct vehicle *node)
{
        if (node->next_vehicle) {
                struct vehicle *next = node->next_vehicle;
                node->next_vehicle = next->next_vehicle;
                free(next);
        }
}

/**
 * clean() - Removes the semaphores from the system before exiting.
 */
void clean()
{
        rm_sem(vehicle_sem);
        rm_sem(bridge_sem);
}
